*** Settings ***
Documentation     A test suite with a single Gherkin style test.
...
...               This test is functionally identical to the example in
...               valid_login.robot file.
Resource          resource_youtube.robot
Test Teardown     Close Browser

*** Variables ***
${key}            chiranjeevi

*** Test Cases ***
Valid Login
    Given Open Browser To YouTube Page
    
    
  

*** Keywords ***
Open Browser To YouTube Page
   :FOR  ${BROWSER}  IN   @{BROWSERS}
    \  log to console  call keyword that does your test with ${BROWSER}
    \  Open Browser  ${URL}  ${BROWSER}
    \  Maximize Browser Window
    \  Set Selenium Speed    ${DELAY}
    \  YouTube Page Should Be Open
    \  User ${key} Search
    \  Submit Search
    \  Singer Page Tilte
    \  Close Browser



User ${key} Search
    Input Keyword  ${key}
    
Singer Page Tilte
    Title Should Be  ${key} - YouTube

YouTube Page Should Be Open
    Title Should Be  YouTube

Submit Search
    Click Button  ${submit}



