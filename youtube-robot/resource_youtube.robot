*** Settings ***
Documentation     A resource file with reusable keywords and variables.
...
...               The system specific keywords created here form our own
...               domain specific language. They utilize keywords provided
...               by the imported Selenium2Library.
Library           Selenium2Library

*** Variables ***
${BROWSER}        firefox       #So default browser is firefox. you can leave it blank
@{BROWSERS}       firefox  chrome 
${DELAY}          0
${URL}            http://www.youtube.com
${WELCOME URL}    http://${URL}/
${Title}          YouTube
${search_field}   xpath=//input[@id='search']
${submit}         xpath=//button[@id='search-icon-legacy']

     

*** Keywords ***
Open Browser To YouTube Page
     :FOR  ${BROWSER}  IN   @{BROWSERS}
    \  log to console  call keyword that does your test with ${BROWSER}
    Open Browser  ${URL}  ${BROWSER}
    Maximize Browser Window
    Set Selenium Speed    ${DELAY}
    YouTube Page Should Be Open
	
YouTube Page Should Be Open
    Title Should Be  YouTube

Go To YouTube Page
    Go To    ${URL}
    YouTube Page Should Be Open

Input Keyword
    [Arguments]  ${search_key}
    Input Text   ${search_field}  ${search_key}

Submit Search
    Click Button  ${submit}  
    

Submit Credentials
    Click Button    login_button

Welcome Page Should Be Open
    Location Should Be    ${WELCOME URL}
    Title Should Be    Welcome Page
    
test with several browser
    [Arguments]    ${browser}
    :FOR  ${browser}  IN   @{BROWSERS}
    \  log to console  call keyword that does your test with ${browser}

