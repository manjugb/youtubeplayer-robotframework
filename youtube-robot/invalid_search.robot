*** Settings ***
Documentation     A test suite containing tests related to invalid Search.
...
...               These tests are data-driven by their nature. They use a single
...               keyword, specified with Test Template setting, that is called
...               with different arguments to cover different scenarios.
...
...               This suite also demonstrates using setups and teardowns in
...               different levels.
Suite Setup       Open Browser To YouTube Page
Suite Teardown    Close Browser
Test Setup        Go To YouTube Page
Test Template     Search With Invalid KeyWord Should Fail
Resource          resource_youtube.robot

*** Variables ***
${ERROR MESSAGE}  xpath=//div[@class='promo-title style-scope ytd-background-promo-renderer']
${results}        xpath=//div[@class='promo-body-text style-scope ytd-background-promo-renderer']


*** Test Cases ***               USER NAME        
invalid Search Key                 ""..,,   
invalid Search Key                 ""
invalid Search Key                 ,$##@,.       

*** Keywords ***
Search With Invalid KeyWord Should Fail
    [Arguments]    ${search_key}    
    Input Keyword   ${search_key}
    Submit Search
    Result Should Displayed
    

Iprice Search Should Have Failed With Message
    Element Text Should Be  ${ERROR MESSAGE}   We couldn't find any items matching your search, check your spelling and filters.
    
Result Should Displayed
    Element Text Should Be    ${results}  Try different keywords or remove search filters
