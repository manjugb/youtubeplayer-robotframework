*** Settings ***
Documentation     A test suite with a single test for valid login.
...
...               This test has a workflow that is created using keywords in
...               the imported resource file.
Resource          resource_youtube.robot


*** Variables ***
${search_key}    ms subbalakshmi  
${key}           sunitha 
${PageTitle}     ${search_key} - 'YouTube'
${PageTitle1}    ${key} - 'YouTube'



*** Test Cases ***  
Valid Search_one 
    Open Browser To YouTube Page
    YouTube Page Should Be Open
    Input Keyword   ${search_key}
    Submit Search
    Title Should Be  ${search_key} - YouTube
    [Teardown]    Close Browser

Valid Search_two 
    Open Browser To YouTube Page
    YouTube Page Should Be Open
    Input Keyword  ${key}
    Submit Search
    Title Should Be  ${key} - YouTube
    [Teardown]    Close Browser





