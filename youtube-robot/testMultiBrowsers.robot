*** Settings ***
Documentation     A resource file with reusable keywords and variables.
...
...               The system specific keywords created here form our own
...               domain specific language. They utilize keywords provided
...               by the imported Selenium2Library.
Library           Selenium2Library

*** Variables ***
${BROWSER}        chrome      #So default browser is firefox. you can leave it blank
@{BROWSERS}       firefox    chrome
${URL}            http://www.youtube.com
${DELAY}          0
${WELCOME URL}    http://${URL}/


*** Test Cases ***
TestBrowser
    test with several browser
    
    
*** Keywords ***
test with several browser
   :FOR  ${BROWSER}  IN   @{BROWSERS}
    \  log to console  call keyword that does your test with ${BROWSER}
    \  Open Browser   ${URL}   ${BROWSER}   
    \  Maximize Browser Window
    \  Set Selenium Speed    ${DELAY}
    \  YouTube Page Should Be Open 
    \  Close Browser 
    
    
  
YouTube Page Should Be Open
    Title Should Be  YouTube
    
    
	    
